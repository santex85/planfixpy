from xml.etree import ElementTree

import requests as requests
from requests.auth import HTTPBasicAuth
from xmltodict3 import XmlTextToDict

from settings import HEADERS, API_URL, URL, API_KEY, TOKEN


class Planfix:
    FINISH_XML = '\n</request>'

    def __init__(self, url=URL, api_key=API_KEY, token=TOKEN):
        self._url = url
        self._api_key = api_key
        self._token = token
        try:
            name = self._url.split('https://')
            self.account = name[1].split('.')[0]
        except IndexError:
            print('Error: check url is correct: "https://name.planfix.ru"')

    def send_request(self, xml):
        response = requests.post(API_URL, data=xml, headers=HEADERS, auth=HTTPBasicAuth(self._api_key, self._token))
        try:
            response = XmlTextToDict(response.text).get_dict()
            return response
        except ElementTree.ParseError:
            response = {
                'response': {
                    '@status': 'error',
                    'message': 'Error parser response. Check request parameters.'
                }
            }
            return response

    @staticmethod
    def gen_start_xml(method: str) -> str:
        """
        returns the generated string for the beginning of the request body in PF
        :rtype: str
        """
        return f'<?xml version="1.0" encoding="UTF-8"?>\n<request method="{method}">\n'
