import os

from xmltodict3 import XmlFileToDict
from planfixpy import Planfix
from planfixpy.task import Task
from utils import get_result, validate_request_action_add, validate_request_action_update


class Action(Planfix):
    """
    Class for working with action(comments)
    """
    description: str = None
    contact_general: int = None
    task_new_status: str = None
    notified_list_user_id: tuple = None
    is_hidden: int = 0
    owner_id: int = None
    date_time: str = None
    analitic_id: int = None
    item_data_field_id: int = None
    item_data_value: str = None
    page_current: int = 1
    page_size: int = 100
    sort: str = 'desc'

    def add(self, task_id: int = None, general: int = None) -> dict:
        """
        adding a comment
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/action/add.xml'), ignore_namespace=True).get_dict()[
            'request']
        request['account'] = self.account
        request['action']['description'] = self.description
        request['action']['task']['id'] = task_id
        request['action']['task']['general'] = general
        request['action']['contact']['general'] = self.contact_general
        request['action']['isHidden'] = self.is_hidden
        request['action']['owner']['id'] = self.owner_id

        validate_request_action_add(self, request)

        return get_result(self, 'action.add', request)

    def get(self, action_id: int) -> dict:
        """
        get comment
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/action/get.xml'), ignore_namespace=True).get_dict()[
            'request']
        request['account'] = self.account
        request['action']['id'] = action_id
        return get_result(self, 'action.get', request)

    def update(self, action_id: int, delete_analitics: tuple = None) -> dict:
        action = self.get(action_id=action_id)

        request = XmlFileToDict(os.path.abspath('templates_xml/action/update.xml'), ignore_namespace=True).get_dict()[
            'request']
        request['account'] = self.account
        request['action']['id'] = action_id
        request['action']['description'] = self.description if self.description else action.get('description')
        request['action']['deletedAnalitics']['key'] = delete_analitics
        validate_request_action_update(self, action, request)
        return get_result(self, 'action.update', request)

    def get_list(self, task_id: int = None, general: int = None):
        # отлавливаем по типу см документацию
        request = XmlFileToDict(os.path.abspath('templates_xml/action/get-list.xml'), ignore_namespace=True).get_dict()[
            'request']
        request['account'] = self.account
        request['task']['id'] = task_id
        request['task']['general'] = general
        request['contact']['general'] = self.contact_general
        request['pageCurrent'] = self.page_current
        request['pageSize'] = self.page_size
        request['sort'] = self.sort
        return get_result(self, 'action.getList', request)

    def get_list_by_period(self, form_date: str = None, to_date: str = None, task_id: int = None):
        request = XmlFileToDict(os.path.abspath('templates_xml/action/get-list-by-period.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['fromDate'] = form_date
        request['toDate'] = to_date
        request['pageCurrent'] = self.page_current
        request['pageSize'] = self.page_size
        request['sort'] = self.sort
        request['task']['id'] = task_id

        return get_result(self, 'action.getListByPeriod', request)

    def get_list_with_analitic(self, analitic_id, task_id: int = None):
        request = XmlFileToDict(os.path.abspath('templates_xml/action/get-list-with-analitic.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['analitic']['id'] = analitic_id
        request['task']['id'] = task_id
        request['pageCurrent'] = self.page_current
        request['pageSize'] = self.page_size

        return get_result(self, 'action.getListWithAnalitic', request)

    def delete(self, action_id: int):
        """
        Функция позволяет удалить комментарий. Её выполнение требует наличие соответствующих прав.
        Также не может быть удален комментарий, которым менялся статус задачи.
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/action/delete.xml'), ignore_namespace=True).get_dict()[
            'request']
        request['account'] = self.account
        request['action']['id'] = action_id

        return get_result(self, 'action.delete', request)
