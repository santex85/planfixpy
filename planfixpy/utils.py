from dict2xml import dict2xml

from planfixpy import Planfix


def validate_request_task_update(self, task, request):
    if not task.get('workers') and not self.workers_users_id and not self.workers_groups_id:
        del request['task']['workers']
    else:
        if task.get('workers'):
            if task['workers'].get('groups'):
                request['task']['workers']['groups']['id'] = self.workers_groups_id if self.workers_groups_id else task[
                    'workers']['groups']['group']['id']
            if task['workers'].get('users'):
                request['task']['workers']['users']['id'] = self.workers_users_id if self.workers_users_id else task[
                    'workers']['users']['user']['id']

    if not task.get('members') and not self.members_users_id and not self.members_groups_id:
        del request['task']['members']
    else:
        if task.get('members'):
            if task['members'].get('groups'):
                request['task']['members']['groups']['id'] = self.members_groups_id if self.members_groups_id else task[
                    'members']['groups']['group']['id']
            if task['members'].get('users'):
                request['task']['members']['users']['id'] = self.members_users_id if self.members_users_id else task[
                    'members']['users']['user']['id']

    if not task.get('auditors') and not self.auditors_users_id and not self.auditors_groups_id:
        del request['task']['auditors']
    else:
        if task.get('auditors'):
            if task['auditors'].get('groups'):
                request['task']['auditors']['groups']['id'] = self.auditors_groups_id if self.auditors_groups_id else \
                    task['auditors']['groups']['group']['id']
            if task['auditors'].get('users'):
                request['task']['auditors']['users']['id'] = self.auditors_users_id if self.auditors_users_id else task[
                    'auditors']['users']['user']['id']

    if not task.get('startDateIsSet') and self.start_date_is_set is None:
        del request['task']['startDate']
    else:
        request['task']['startDateIsSet'] = self.start_date_is_set if self.start_date_is_set else task.get(
            'startDateIsSet')
        request['task']['startDate'] = self.start_date if self.start_date else task.get('startDate')

    if not task.get('startTimeIsSet') and self.start_time_is_set is None:
        del request['task']['startTime']
    else:
        request['task']['startTimeIsSet'] = self.start_time_is_set if self.start_time_is_set else task.get(
            'startTimeIsSet')
        request['task']['startTime'] = self.start_time if self.start_time else task.get('startTime')

    if not task.get('endDateIsSet') and self.end_date_is_set is None:
        del request['task']['endDate']
    else:
        request['task']['endDateIsSet'] = self.end_date_is_set if self.end_date_is_set else task.get('endDateIsSet')
        request['task']['endDate'] = self.end_date if self.end_date else task.get('endDate')

    if not task.get('endTimeIsSet') and self.end_time_is_set is None:
        del request['task']['endTime']
    else:
        request['task']['endTimeIsSet'] = self.end_time_is_set if self.end_time_is_set else task.get('endTimeIsSet')
        request['task']['endTime'] = self.end_time if self.end_time else task.get('endTime')

    if not task.get('durationIsSet') and self.duration_is_set is None:
        del request['task']['duration']
        del request['task']['durationUnit']
        del request['task']['durationIsSet']

    else:
        request['task']['duration'] = self.duration if self.duration else task.get('duration')
        request['task']['durationUnit'] = self.duration_unit if self.duration_unit else task.get('durationUnit')
        request['task']['durationIsSet'] = self.duration_is_set if self.duration_is_set else task.get('durationIsSet')

    return request


def validate_request_task_list(self, request):
    """
    Checks a dictionary for empty values
    :param self: task object
    :param request: dictionary with data to be validated
    :return: a dictionary with no empty values
    """
    if self.project_id is None:
        del request['project']['id']
    else:
        request['project']['id'] = self.project_id

    if self.project_with_subprojects is None:
        del request['project']['withSubprojects']
    else:
        request['project']['withSubprojects'] = self.project_with_subprojects

    if self.project_id is None and self.project_with_subprojects is None:
        del request['project']

    if self.status is None:
        del request['status']
    else:
        request['status'] = self.status

    if self.parent_id is None:
        del request['parent']
    else:
        request['parent']['id'] = self.parent_id

    if self.user_id is None:
        del request['user']
    else:
        request['user']['id'] = self.user_id

    if self.filters is None:
        del request['filters']
    else:
        request['filters'] = self.filters

    return request


def validate_request_action_add(self, request):
    """
    Checks a dictionary for empty values
    :param self: action object
    :param request: dictionary with data to be validated
    :return: a dictionary with no empty values
    """
    if self.task_new_status is None:
        del request['action']['taskNewStatus']
    else:
        request['action']['taskNewStatus'] = self.task_new_status
    if self.notified_list_user_id is None:
        del request['action']['notifiedList']['user']['id']
    else:
        request['action']['notifiedList']['user']['id'] = self.notified_list_user_id
    if self.date_time is None:
        del request['action']['dateTime']
    else:
        request['action']['dateTime'] = self.date_time
    if self.analitic_id is None:
        del request['action']['analitics']
    else:
        request['action']['analitics']['analitic']['id'] = self.analitic_id
        request['action']['analitics']['analitic']['analiticData']['itemData']['fieldId'] = self.item_data_field_id
        request['action']['analitics']['analitic']['analiticData']['itemData']['value'] = self.item_data_value

    return request


def validate_request_action_update(self, action, request):
    """
    Checks a dictionary for empty values
    """
    if self.task_new_status is None and not action.get('taskNewStatus'):
        del request['action']['taskNewStatus']
    else:
        request['action']['taskNewStatus'] = self.task_new_status if self.task_new_status else action.get(
            'taskNewStatus')

    if not self.is_hidden and not action.get('isHidden'):
        del request['action']['isHidden']
    else:
        request['action']['isHidden'] = self.is_hidden if self.is_hidden else action.get('isHidden')

    if self.notified_list_user_id is None and not action.get('notifiedList'):
        del request['action']['notifiedList']
    else:
        request['action']['notifiedList']['user']['id'] = self.notified_list_user_id if self.notified_list_user_id \
            else action.get('notifiedList')

    if self.date_time is None and not action.get('dateTime'):
        del request['action']['dateTime']
    else:
        request['action']['dateTime'] = self.date_time if self.date_time else action.get('dateTime')

    if self.analitic_id is None and not action.get('analitics'):
        del request['action']['analitics']
    else:
        if action.get('analitics'):
            request['action']['analitics']['analitic']['id'] = self.analitic_id if self.analitic_id else \
                action['analitics']['analitic'].get('id')

            item_data = request['action']['analitics']['analitic']['analiticData']['itemData']

            item_data['fieldId'] = self.item_data_field_id if self.item_data_field_id else \
                action['analitics']['analitic']['analiticData']['itemData'].get('fieldId')

            item_data['value'] = self.item_data_value if self.item_data_value else \
                action['analitics']['analitic']['analiticData']['itemData'].get('value')

        request['action']['analitics']['analitic']['id'] = self.analitic_id
        request['action']['analitics']['analitic']['analiticData']['itemData']['fieldId'] = self.item_data_field_id
        request['action']['analitics']['analitic']['analiticData']['itemData']['value'] = self.item_data_value


def get_result(self: 'Planfix', pf_function_name: str, request: dict):
    """
    gets the name of the function in the API and the request. Returns the result from the API
    :param pf_function_name: function name in PF API
    :param request:
    :param self:
    :return: result
    """
    xml = f"{Planfix.gen_start_xml(pf_function_name)}{dict2xml(request, indent='  ')}{Planfix.FINISH_XML}"

    response = self.send_request(xml.encode('utf-8'))

    if response['response']['@status'] != 'ok':
        return response['response']

    return response['response']
