import os

from xmltodict3 import XmlFileToDict

from planfixpy import Planfix
from settings import URL, API_KEY, TOKEN
from utils import get_result


class Project(Planfix):
    """
    class for interacting with projects PF
    """
    title: str = None
    description: str = None
    user_id: int or str = None
    owner_id: int = None
    client_id: int = None
    status: str = 'ACTIVE'
    hidden: bool = False
    has_end_date: bool = False
    end_date: str = ''
    group_id: str or int = None
    parent_id: str or int = None
    auditors_id: tuple = ()
    managers_id: tuple = ()
    workers_id: tuple = ()
    custom_data: list = {}
    target: str = 'all'
    sort_type: str = 'TITLE_ASC'
    page_current: int = 1
    page_size: int = 100
    filters: 'filters' = None

    def add(self) -> dict:
        """
        creates PF project
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/project/add.xml'), ignore_namespace=True).get_dict()[
            'request']

        request['account'] = self.account
        request['project']['title'] = self.title
        request['project']['description'] = self.description
        request['project']['owner']['id'] = self.owner_id
        request['project']['client']['id'] = self.client_id
        request['project']['status'] = self.status
        request['project']['hidden'] = self.hidden
        request['project']['hasEndDate'] = self.has_end_date
        request['project']['endDate'] = self.end_date
        request['project']['group']['id'] = self.group_id
        request['project']['parent']['id'] = self.parent_id
        request['project']['auditors']['id'] = self.auditors_id
        request['project']['managers']['id'] = self.managers_id
        request['project']['workers']['id'] = self.workers_id
        request['project']['customData'] = self.custom_data

        return get_result(self, 'project.add', request)

    def get(self, project_id: int = None, general: int = None) -> dict:
        """
        Method allows you to get information about the project
        """
        request = XmlFileToDict('templates_xml/project/get.xml', ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['project']['id'] = project_id
        request['project']['general'] = general

        return get_result(self, 'project.get', request)

    def update(self, project_id: int = None, general: int = None):
        """
        Method for updating project data
        :return:
        """
        if project_id:
            project_ = self.get(project_id=project_id)
            if project_['@status'] == 'error':
                return project_
            project_ = project_['project']
        elif general:
            project_ = self.get(general=general)
            if project_['@status'] == 'error':
                return project_
            project_ = project_['task']
        else:
            return Exception('Error: Must be entered from parameters task_id or general')

        request = XmlFileToDict(os.path.abspath('templates_xml/project/update.xml'), ignore_namespace=True).get_dict()[
            'request']
        request['account'] = self.account
        request['project']['id'] = project_id
        request['project']['general'] = general
        request['project']['title'] = self.title if self.title else project_['title']
        request['project']['description'] = self.description if self.description else project_['description']
        request['project']['owner']['id'] = self.owner_id if self.owner_id else project_['owner']['id']
        request['project']['client']['id'] = self.client_id if self.client_id else project_['client']['id']
        request['project']['status'] = self.status if self.status else project_['status']
        request['project']['hidden'] = self.hidden if self.hidden else project_['hidden']
        request['project']['hasEndDate'] = self.has_end_date if self.has_end_date else project_['hasEndDate']
        request['project']['group']['id'] = self.group_id if self.group_id else project_['group']['id']
        request['project']['parent']['id'] = self.parent_id if self.parent_id else project_['parent']['id']
        request['project']['auditors']['id'] = self.auditors_id if self.auditors_id else project_.get('auditors')
        request['project']['managers']['id'] = self.managers_id if self.managers_id else project_.get('managers')
        request['project']['workers']['id'] = self.workers_id if self.workers_id else project_.get('workers')
        request['project']['customData'] = self.custom_data if self.custom_data else project_.get('customData')
        if request['project']['hasEndDate'] == '0':
            del request['project']['endDate']
        else:
            request['project']['endDate'] = self.end_date if self.end_date else project_.get('endDate')

        return get_result(self, 'project.update', request)

    def get_list(self) -> dict:
        """
        Method for getting a list of projects
        :return: list projects
        """
        request = XmlFileToDict('templates_xml/project/get_list.xml', ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['user']['id'] = self.user_id
        request['target'] = self.target
        request['status'] = self.status
        request['sortType'] = self.sort_type
        request['pageCurrent'] = self.page_current
        request['pageSize'] = self.page_size
        request['client']['id'] = self.client_id
        request['filters'] = self.filters

        if request['user']['id'] is None:
            del request['user']
        if request['client']['id'] is None:
            del request['client']

        return get_result(self, 'project.getList', request)


project = Project()
project.filters = {
    'filter': [{
        'type': 5005, 'operator': 'equal', 'value': 'tomorrow'
    }]
}
get_list_project = project.get_list()
print(get_list_project)
