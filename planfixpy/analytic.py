import os

from xmltodict3 import XmlFileToDict

from planfixpy import Planfix
from utils import get_result


class Analytic(Planfix):
    """
    Analytics class
    """

    def get_group_list(self):
        """
        Request for a list of analytics groups
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/analytic/get-group-list.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account

        return get_result(self, 'analitic.getGroupList', request)

    def get_list(self, analytic_group_id: int = None):
        """
        Request for a list of analytics
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/analytic/get-list.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['analiticGroup']['id'] = analytic_group_id

        return get_result(self, 'analitic.getList', request)

    def get_options(self, analytic_id: int):
        """
        Getting a reference for analytics.
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/analytic/get-options.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['analitic']['id'] = analytic_id

        return get_result(self, 'analitic.getOptions', request)

    def get_handbook(self, handbook_id: int):
        request = XmlFileToDict(os.path.abspath('templates_xml/analytic/get-handbook.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['handbook']['id'] = handbook_id

        return get_result(self, 'analitic.getHandbook', request)

    def get_data(self, analytic_keys: tuple):
        """
        :param analytic_keys идентификаторы строки данных аналитики возвращается функцией action.get
        Get analytic data by attached action. The list of available analytics is obtained from the list of actions.
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/analytic/get-data.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['analiticKeys']['key'] = analytic_keys

        return get_result(self, 'analitic.getData', request)

    def get_data_by_condition(self, analytic_id: int, task_id: int = None, general: int = None,
                              page_current: int = 1, page_size: int = 100, field: int = None, from_date: str = None,
                              to_date: str = None, user_id: int = None):
        """
        Receiving analyst data according to a specified condition.
        :param analytic_id: идентификатор аналитики
        :param task_id: идентификатор задачи
        :param general: номер задачи
        :param page_current: страница
        :param page_size:размер страницы (максимум 100)
        :param field: идентификатор поля аналитики по которому делается условие
        :param from_date: для условия по дате - от даты
        :param to_date: для условия по дате - до даты
        :param user_id: для условия по полю типа Список пользователей / Сотрудник / Группа, сотрудник или контакт -
        идентификатор сотрудника, как он возвращается функцией User.get_ist
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/analytic/get-data-by-condition.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['pageSize'] = page_size
        request['pageCurrent'] = page_current
        request['analitic']['id'] = analytic_id
        request['task']['id'] = task_id
        request['task']['general'] = general
        request['filters']['filter']['field'] = field
        request['filters']['filter']['fromDate'] = from_date
        request['filters']['filter']['toDate'] = to_date
        request['filters']['filter']['userid'] = user_id

        return get_result(self, 'analitic.getDataByCondition', request)
