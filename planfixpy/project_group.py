import os

from xmltodict3 import XmlFileToDict

from planfixpy import Planfix
from utils import get_result


class ProjectGroup(Planfix):
    name: str = None

    def add(self):
        """
        Creating a project group
        :return:
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/project-group/add-group.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['projectGroup']['name'] = self.name

        return get_result(self, 'projectGroup.add', request)

    def get(self, group_id: int):
        """
        Getting a group of projects
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/project-group/get.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['projectGroup']['id'] = group_id

        return get_result(self, 'projectGroup.get', request)

    def update(self, group_id):
        """
        Updating a project group
        """
        if group_id:
            group_ = self.get(group_id=group_id)
            if group_['@status'] == 'error':
                return group_
            group_ = group_['projectGroup']
        else:
            return Exception('Error: Must be entered from parameters task_id or general')

        request = XmlFileToDict(os.path.abspath('templates_xml/project-group/update.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['projectGroup']['id'] = group_id
        request['projectGroup']['name'] = self.name if self.name else group_['projectGroup']['name']

        return get_result(self, 'projectGroup.update', request)

    def move(self, move_id: int, target_id: int):
        """
        Moving a group of group
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/project-group/update.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['move']['projectGroup']['id'] = move_id
        request['after']['projectGroup']['id'] = target_id

        return get_result(self, 'projectGroup.move', request)

    def get_list(self):
        """
        Getting a list of a project group
        """
        request = {'account': self.account}

        return get_result(self, 'projectGroup.getList', request)
