import os

from xmltodict3 import XmlFileToDict

from planfixpy import Planfix
from task import Task
from utils import get_result


class TaskStatus(Planfix):

    def get_set_list(self) -> list:
        """
        Method for getting a list of task processes.
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/task-status/get-set-list.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        return get_result(self, 'taskStatus.getSetList', request)

    def get_list_of_set(self, task_status_set_id: int):
        """
        Method for getting a list of process statuses. Returns all statuses that are present in a set of process
        statuses.
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/task-status/get-list-of-set.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['taskStatusSet']['id'] = task_status_set_id

        return get_result(self, 'taskStatus.getListOfSet', request)
