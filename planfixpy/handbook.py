import os

from xmltodict3 import XmlFileToDict

from planfixpy import Planfix
from utils import get_result


class Handbook(Planfix):
    record_archived: int = None
    record_is_group: int = None
    record_name: str = None
    record_parent_key: int = None
    page_current: int = 1
    page_size: int = 100
    custom_data: list = None

    def get_group_list(self) -> list:
        """
        Request for a list of directory groups
        """
        request = {'account': self.account}

        return get_result(self, 'handbook.getGroupList', request)

    def get_list(self, group_id: int = None) -> list:
        """
        Request for a list of directories
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/handbook/get-list.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['group']['id'] = group_id

        return get_result(self, 'handbook.getList', request)

    def get_structure(self, handbook_id: int = None) -> list:
        """
        Getting the description of the reference - the full content of the fields and their value types
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/handbook/get-structure.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['handbook']['id'] = handbook_id

        return get_result(self, 'handbook.getStructure', request)

    def get_records(self, handbook_id: int, group_id: int = None) -> list:
        """
        Get directory records.
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/handbook/get-records.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['handbook']['id'] = handbook_id
        request['parentKey'] = group_id
        request['pageCurrent'] = self.page_current
        request['pageSize'] = self.page_size

        return get_result(self, 'handbook.getRecords', request)

    def get_record(self, handbook_id: int, record_key: int):
        """
        Get directory record.
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/handbook/get-record.xml'),
                                ignore_namespace=True).get_dict()['request']

        request['account'] = self.account
        request['handbook']['id'] = handbook_id
        request['key'] = record_key

        return get_result(self, 'handbook.getRecord', request)

    def get_record_multi(self, handbook_id, records: tuple):
        """
        The function of getting a set of directory records by their identifiers. Allows to get data up to 100 records
        per request.
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/handbook/get-record-multi.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['handbook']['id'] = handbook_id
        request['records']['key'] = records

        return get_result(self, 'handbook.getRecordMulti', request)

    def add_record(self, handbook_id: int, parent_key: int = None):
        request = XmlFileToDict(os.path.abspath('templates_xml/handbook/add-record.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['handbook']['id'] = handbook_id
        request['parentKey'] = parent_key
        request['isGroup'] = self.record_is_group
        request['archived'] = self.record_archived
        request['name'] = self.record_name
        # TODO generate custom_date
        request['customData']['customValue'] = self.custom_data

        return get_result(self, 'handbook.addRecord', request)

    def update_record(self, handbook_id: int, record_key: int):
        record = self.get_record(handbook_id=handbook_id, record_key=record_key)
        request = XmlFileToDict(os.path.abspath('templates_xml/handbook/update-record.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['handbook']['id'] = handbook_id
        request['key'] = record_key
        request['parentKey'] = self.record_parent_key if self.record_parent_key else record.get('parentKey')
        request['isGroup'] = self.record_is_group if self.record_is_group else record.get('isGroup')
        request['name'] = self.record_name if self.record_name else record.get('name')
        request['archived'] = self.record_archived if self.record_archived else record.get('archived')
        request['customData'] = self.custom_data if self.custom_data else record.get('customData')

        return get_result(self, 'handbook.updateRecord', request)

    @staticmethod
    def generate_custom_data(field_id: int = None, file_name: str = None, file_source_type: str = 'FILESYSTEM',
                             file_other_url: str = None, file_body_text: str = None, file_description: str = None,
                             new_version: bool = False):
        custom_date = {'id': field_id,
                       'files': {'file': {'name': file_name, 'sourceType': file_source_type,
                                          'otherFile': {'url': file_other_url}, 'body': file_body_text,
                                          'description': file_description, 'newversion': new_version}}},
        return custom_date
