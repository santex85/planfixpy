import os

from xmltodict3 import XmlFileToDict

from planfixpy import Planfix
from utils import validate_request_task_list, get_result, validate_request_task_update


class Task(Planfix):
    """
    class for working with tasks
    """
    template: int = None
    title: str = None
    description: str = None
    importance: str = None
    status: str or int = None  # DRAFT or ACTIVE or DELAYED
    status_set: int = None
    check_result: int = None  # whether the task is a task with mandatory check of the result
    user_id: int = None
    owner_id: int = None
    parent_id: int = None
    project_id: int = None
    project_with_subprojects: int = None
    client_id: int = None
    begin_date_time: str = ''
    start_date_is_set: int = None
    start_date: str = None
    start_time_is_set: int = None
    start_time: str = None
    end_date_is_set: int = None
    end_date: str = None
    end_time_is_set: int = None
    end_time: str = None
    is_summary: int = None
    duration_is_set: int = None
    duration: int = None
    duration_unit: int = None
    workers_users_id: tuple = ()
    workers_groups_id: tuple = ()
    members_users_id: tuple = ()
    members_groups_id: tuple = ()
    auditors_users_id: tuple = ()
    auditors_groups_id: tuple = ()
    custom_data: list = {}
    target: int or str = 'all'
    sort: str = 'NUMBER_ASC'
    page_current: int = 1
    page_size: int = 100
    filter: str = ''
    filters: list = None
    add_workers_users_id: tuple = ()
    del_workers_users_id: tuple = ()
    add_workers_groups_id: tuple = ()
    del_workers_groups_id: tuple = ()

    def add(self) -> list:
        """
        Method allows you to create a new task
        :return: id created task
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/task/add.xml'), ignore_namespace=True).get_dict()[
            'request']
        request['account'] = self.account
        request['task']['template'] = self.template
        request['task']['title'] = self.title
        request['task']['description'] = self.description
        request['task']['importance'] = 'AVERAGE'
        request['task']['status'] = self.status
        request['task']['statusSet'] = self.status_set
        request['task']['checkResult'] = self.check_result
        request['task']['owner']['id'] = self.owner_id
        request['task']['parent']['id'] = self.parent_id
        request['task']['project']['id'] = self.project_id
        request['task']['client']['id'] = self.client_id
        request['task']['beginDateTime'] = self.begin_date_time
        request['task']['startDateIsSet'] = self.start_date_is_set
        request['task']['startDate'] = self.start_date
        request['task']['startTimeIsSet'] = self.start_time_is_set
        request['task']['startTime'] = self.start_time
        request['task']['endDateIsSet'] = self.end_date_is_set
        request['task']['endDate'] = self.end_date
        request['task']['endTimeIsSet'] = self.end_time_is_set
        request['task']['endTime'] = self.end_time
        request['task']['isSummary'] = self.is_summary
        request['task']['duration'] = self.duration
        request['task']['durationUnit'] = self.duration_unit
        request['task']['durationIsSet'] = self.duration_is_set
        request['task']['workers']['users']['id'] = self.workers_users_id
        request['task']['workers']['groups']['id'] = self.workers_groups_id
        request['task']['members']['users']['id'] = self.members_users_id
        request['task']['members']['groups']['id'] = self.members_groups_id
        request['task']['auditors']['users']['id'] = self.auditors_user_id
        request['task']['auditors']['groups']['id'] = self.auditors_groups_id
        request['task']['customData'] = self.custom_data

        return get_result(self, 'task.add', request)

    def get(self, task_id: int = None, general: int = None) -> list:
        """
        Method of receiving a task card.
        :param task_id: required
        or
        :param general: required
        :return: list with task
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/task/get.xml'), ignore_namespace=True).get_dict()[
            'request']
        request['account'] = self.account
        request['task']['id'] = task_id
        request['task']['general'] = general

        return get_result(self, 'task.get', request)

    def update(self, task_id: int = None, general: int = None, silent: int = 0) -> list or Exception:
        """
        Task information update method
        :param task_id: required
        or
        :param general: required
        :param silent: notify or without notification
        :return:  id of the updated task
        """

        if task_id:
            task_ = self.get(task_id=task_id)
            if task_['@status'] == 'error':
                return task_
            task_ = task_['task']
        elif general:
            task_ = self.get(general=general)
            if task_['@status'] == 'error':
                return task_
            task_ = task_['task']
        else:
            return Exception('Error: Must be entered from parameters task_id or general')

        request = XmlFileToDict(os.path.abspath('templates_xml/task/update.xml'), ignore_namespace=True).get_dict()[
            'request']
        #  update or leave the field unchanged
        request['account'] = self.account
        request['silent'] = silent
        request['task']['id'] = task_id
        request['task']['general'] = general

        request['task']['title'] = self.title if self.title else task_.get('title')
        request['task']['description'] = self.description if self.description else task_.get('description')
        request['task']['importance'] = self.importance if self.importance else task_.get('importance')
        request['task']['status'] = self.status if self.status else task_.get('status')
        request['task']['checkResult'] = self.check_result if self.check_result is not None else task_.get('checkResult')
        request['task']['owner']['id'] = self.owner_id if self.owner_id else task_.get('owner')['id']
        request['task']['parent'] = self.parent_id if self.parent_id else task_.get('parent')['id']
        request['task']['project']['id'] = self.project_id if self.project_id else task_.get('project')['id']
        request['task']['client']['id'] = self.client_id if self.client_id else task_.get('client')['id']
        request['task']['isSummary'] = self.is_summary if self.is_summary is not None else task_.get('isSummary')
        request['task']['statusSet'] = self.status_set = self.status_set if self.status_set else task_.get('statusSet')

        request['task']['customData'] = self.custom_data if self.custom_data else task_.get('customData')

        validate_request_task_update(self, task_, request)

        return get_result(self, 'task.update', request)

    def update_custom_fields(self, task_id: int = None, general: int = None) -> list:
        """
        Method for updating custom task fields. It can be used if the user does not have permission to modify the
        task and therefore cannot use the update function, but he does have permission to modify custom fields.
        :param task_id: required
        or
        :param general: required
        :return:
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/task/update-custom-fields.xml'),
                                ignore_namespace=True).get_dict()['request']

        request['account'] = self.account
        request['task']['id'] = task_id
        request['task']['general'] = general
        request['task']['customData'] = self.custom_data

        return get_result(self, 'task.updateCustomData', request)

    def get_multi(self, ids_task: tuple = (), generals: tuple = ()) -> list:
        """
        Method of receiving multiple task cards. Allows you to receive data for up to 100 tasks per request
        :param ids_task: required
        or
        :param generals: required
        :return: list tasks
        """

        request = XmlFileToDict(os.path.abspath('templates_xml/task/get-multi.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['tasks']['id'] = ids_task
        request['tasks']['general'] = generals

        return get_result(self, 'task.getMulti', request)

    def get_list(self) -> list:
        """
        Method for getting a list of tasks. Depending on the values of the parameters, you can get a list of tasks
        sorted by different criteria.
        :return:
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/task/get-list.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['target'] = self.target
        request['sort'] = self.sort
        request['pageCurrent'] = self.page_current
        request['pageSize'] = self.page_size
        request['filter'] = self.filter
        request = validate_request_task_list(self, request)

        return get_result(self, 'task.getList', request)

    def accept(self, task_id: int) -> list:
        """
        For further work with the task, the user must accept the task.
        """
        request = XmlFileToDict('templates_xml/task/accept.xml', ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['task']['id'] = task_id

        return get_result(self, 'task.accept', request)

    def reject(self, task_id: int) -> list:
        """
        Rejecting a task
        """
        request = XmlFileToDict('templates_xml/task/reject.xml', ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['task']['id'] = task_id

        return get_result(self, 'task.reject', request)

    def change_expect_date(self, task_id, expect_date) -> list:
        """
        If the user, for some reason, cannot complete the task within the specified time limit, he can postpone the
        execution time.
        """
        request = XmlFileToDict('templates_xml/task/change-expect-date.xml', ignore_namespace=True).get_dict()[
            'request']
        request['account'] = self.account
        request['task']['id'] = task_id
        request['expectDate'] = expect_date

        return get_result(self, 'task.changeExpectDate', request)

    def change_status(self, task_id: int = None, general: int = None, date_time: str = None) -> list:
        """
        change task status
        """
        request = XmlFileToDict(os.path.abspath('templates_xml/task/change_status.xml'),
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['task']['id'] = task_id
        request['task']['general'] = general
        request['status'] = self.status
        request['dateTime'] = date_time

        return get_result(self, 'task.changeStatus', request)

    def get_possible_status_to_change(self, task_id: int) -> list:
        """
        The function allows you to get a list of available statuses for the function task.change_status
        :return:
        """
        request = XmlFileToDict('templates_xml/task/get-possible-status-to-change.xml',
                                ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['task']['id'] = task_id

        return get_result(self, 'task.getPossibleStatusToChange', request)

    def change_workers(self, task_id: int) -> list:
        """
        The method is used to change the list of task executors
        :param task_id:
        :return: result
        """
        request = XmlFileToDict('templates_xml/task/change-workers.xml', ignore_namespace=True).get_dict()['request']
        request['account'] = self.account
        request['task']['id'] = task_id
        request['task']['workers']['addUsers']['id'] = self.add_workers_users_id
        request['task']['workers']['addGroups']['id'] = self.add_workers_groups_id
        request['task']['workers']['delUsers']['id'] = self.del_workers_users_id
        request['task']['workers']['delGroups']['id'] = self.del_workers_groups_id

        return get_result(self, 'task.changeWorkers', request)

    def get_filter_list(self) -> list:
        request = XmlFileToDict('templates_xml/task/get-filter-list.xml', ignore_namespace=True).get_dict()['request']
        request['account'] = self.account

        return get_result(self, 'task.getFilterList', request)
